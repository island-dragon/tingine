# Tingine
JS/TS tiny game engine.


## Usage

### Javascript
```javascript
```

```sh
Output should be
```


### TypeScript
```typescript
```

```sh
Output should be
```


### AMD
```javascript
```


## Test
```sh
npm run test
npm run coverage
```


## Docs
```sh
npm run doc
```


## Resources

- [Test Results](https://island-dragon.gitlab.io/tingine/test_doc/)
- [Code Coverage](https://island-dragon.gitlab.io/tingine/coverage/)
- [RequireJS Sample](https://island-dragon.gitlab.io/tingine/samples/requirejs)
