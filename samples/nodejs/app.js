const requirejs = require('requirejs')
requirejs.config({
  baseUrl: '../../dist',
  paths: {
    src: '../samples/nodejs'
  },
  nodeRequire: require
})

requirejs(['src/game']);
