import { Position2D } from '../../lib/Components/Position2D'
import { Engine } from '../../lib/Core/Engine'
import { System } from '../../lib/Core/System'
import { logger } from '../../lib/Extras/Logger'

class MoveSystem extends System {
  public components = [Position2D]

  public update(dt: number, _ts: number, _entity: number, position: Position2D) {
    position.point.x += 1 * dt
    position.point.y += 1 * dt
    logger.log(position.point)
  }
}

const engine = new Engine()

engine.addEntity([new Position2D(0, 0)])

engine.addUpdateSystem(MoveSystem)
engine.start()
