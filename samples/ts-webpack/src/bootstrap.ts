import { Engine, logger, LogLevel, Position2D } from '../../../dist'
import { Rotation2D } from '../../../dist/Components/Rotation2D'
import GraphicsEngine from '../../../dist/Extras/Graphics/GraphicsEngine'
import GraphicsViewComponent from '../../../dist/Extras/Graphics/GraphicsViewComponent'
import { SpriteSheet } from '../../../dist/Extras/Graphics/SpriteSheet'
import ViewType from '../../../dist/Extras/Graphics/ViewType'
import Input from '../../../dist/Extras/Input/Input'
import { InputComponent } from '../../../dist/Extras/Input/InputComponent'
import { FaceGotoSystem } from '../../../dist/Extras/Maths/FaceGotoSystem'
import GoToComponent from '../../../dist/Extras/Maths/GoToComponent'
import GotoSystem from '../../../dist/Extras/Maths/GotoSystem'
import StatusBar from '../../../dist/Extras/StatusBar/StatusBar'
import { UI } from '../../../dist/Extras/UI/UI'

import interface_pack_data from '../resources/interface_pack.json'
import interface_pack from '../resources/interface_pack.png'
import styles from '../resources/styles.sass'

logger.SetLevel(LogLevel.Trace)

const engine = new Engine()
engine.start()

const graphics = new GraphicsEngine(engine)
graphics.canvas.className = styles.canvas
graphics.clearColor = '#222222'

const statusBar = new StatusBar(engine)
statusBar.dom.className = styles.status

const input = new Input(engine, graphics.canvas)
input.start()

const ui = new UI(engine)
ui.start()

const interfacePackSpriteSheet = new SpriteSheet(interface_pack, interface_pack_data)
interfacePackSpriteSheet.img.className = 'as'

const sprite = interfacePackSpriteSheet.data.frames['panel_woodDetail_blank.png']

const skin = {
  buttonBgColor: '#A3703A',
  buttonFont: 'DroidSansMono',
  buttonFontSize: 14,
  buttonLabelColor: 'white',
  buttonSprite: 'panel_woodDetail.png',
  labelFontColor: '#A3703A',
  labelFontDecor: '',
  labelFontFace: 'DroidSansMono',
  labelFontSize: 14,
  panelBgColor: '#FFF1D2DD',
  panelSprite: 'panel_woodDetail_blank.png',
}

const pos = new Position2D(100, 100)

input.register('mousemove', (e: MouseEvent) => {
  pos.point.set(e.x, e.y)
})

const targetId = engine.addEntity([
  pos,
  new GraphicsViewComponent(ViewType.Text, {
    color: skin.labelFontColor,
    fontDecorations: '',
    fontFace: skin.labelFontFace,
    fontModifier: skin.labelFontDecor,
    fontSize: skin.labelFontSize,
    label: 'Test text',
    maxWidth: '250',
  }, 10),
])

const moving = engine.addEntity([
  new Position2D(100, 100),
  new Rotation2D(0),
  new GoToComponent(targetId, 0.1),
  new GraphicsViewComponent(ViewType.RawSprite, {
    dh: 22,
    dw: 22,
    img: interfacePackSpriteSheet.img,
    raw: true,
    sh: 22,
    sw: 22,
    sx: sprite.frame.x,
    sy: sprite.frame.y,
  }, 10),
])

input.register('click', () => {
  engine.addComponent(moving, new GoToComponent(targetId, 0.1))
})

engine.addUpdateSystem(GotoSystem)
engine.addUpdateSystem(FaceGotoSystem)

ui.setUI((i: InputComponent, ge: GraphicsEngine) => {
  if (i.leftDown) {
    ge.drawCircle(i.x, i.y, 30, 'red')
  }
})
