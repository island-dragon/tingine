# TypeScript WebPack sample

Simple full game sample using tingine.

## Bootstrapping

- npm install
- npm start
- visit localhost:3000


## Important Notes

This sample does not use the tingine package. Instead it uses relative paths to
link in the library code. Every import for tingine can be converted to using the
npm package by simply replacing the prefix of the import:

From:
import GraphicsViewComponent from '../../dist/Extras/Graphics/GraphicsViewComponent'

To:
import GraphicsViewComponent from 'tingine/dist/Extras/Graphics/GraphicsViewComponent'
