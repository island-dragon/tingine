define([
  'Extras/Logger',
  'Core/Engine',
  'Core/System',
  'Components/Parent',
  'Components/Position2D',
  'Extras/Graphics/GraphicsEngine',
  'Extras/Graphics/GraphicsViewComponent',
  'Extras/Graphics/ViewType',
  'Extras/StatusBar/StatusBar',
], (
  { logger, LogLevel },
  { Engine },
  { System },
  { Parent },
  { Position2D },
  { default: GraphicsEngine },
  { default: GraphicsViewComponent },
  { default: ViewType },
  { default: StatusBar },
  ) => {
    logger.SetLevel(LogLevel.Error)

    class OrbitComponent {
      constructor(
        radius,
        speed,
        position,
      ) {
        this.radius = radius
        this.speed = speed
        this.position = position || 0
      }
    }

    class OrbitSystem extends System {
      constructor() {
        super(...arguments)
        this.components = [OrbitComponent, Position2D]
      }

      update(
        dt,
        ts,
        entity,
        orbit,
        position
      ) {
        if (orbit.radius === 0) {
          return
        }

        orbit.position += orbit.speed * dt
        position.point.x = Math.sin(orbit.position) * orbit.radius
        position.point.y = Math.cos(orbit.position) * orbit.radius
      }
    }

    const engine = new Engine()
    const graphics = new GraphicsEngine(engine)
    const statusBar = new StatusBar(engine)

    const newBody = (
      bodyRadius,
      bodyColor,
      parent,
      orbitRadius,
      orbitSpeed,
      x = 0,
      y = 0
    ) => {
      const e = engine.addEntity([
        new Position2D(x, y),
        new OrbitComponent(orbitRadius, orbitSpeed),
        new GraphicsViewComponent(ViewType.Circle, {
          color: bodyColor,
          radius: bodyRadius,
        })
      ])

      if (parent !== null) {
        engine.addComponent(e, new Parent(parent))
      }

      return e
    }


    const sun = newBody(25, 'yellow', null, 0, 0, 350, 350)

    // MERCURY
    newBody(10, 'red', sun, 50, 0.003)

    // VENUS
    newBody(15, 'darkseagreen', sun, 90, 0.002)

    // THE EARTH (92Mmi, 67Kmph)
    const earth = newBody(20, 'blue', sun, 150, 0.001)

    // THE MOON (216Kmi, 2288mph)
    newBody(7, 'gray', earth, 30, 0.0033)

    // MARS
    const mars = newBody(17, 'orange', sun, 225, 0.0007)

    // PHOBOS
    newBody(5, 'gray', mars, 25, 0.004)

    // DEIMOS
    newBody(3, 'gray', mars, 35, 0.005)

    engine.addUpdateSystem(OrbitSystem)
    engine.start()
  })
