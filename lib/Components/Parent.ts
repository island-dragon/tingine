import { Position2D } from './Position2D'

export class Parent {
  constructor(
    public value: number,
    public globalPoint: Position2D = new Position2D(0, 0),
  ) { }

  public toString = () => `Parent(${this.value})`
}
