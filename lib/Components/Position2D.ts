import V2D from '../Extras/Maths/V2D'

export class Position2D {
  public point: V2D
  public lastPoint: V2D

  constructor(
    x: number = 0,
    y: number = 0,
    public relative?: V2D
  ) {
    this.point = new V2D(x, y, relative)
    this.lastPoint = new V2D(0, 0)
  }

  public toString = () => `Position(${this.point.x}, ${this.point.y})`
}
