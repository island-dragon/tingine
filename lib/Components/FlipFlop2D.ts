export class FlipFlop2D {
  constructor(
    public flip: boolean = false,
    public flop: boolean = false,
  ) { }
}
