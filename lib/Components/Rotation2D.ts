import V2D from '../Extras/Maths/V2D'

export class Rotation2D {
  constructor(
    public value: number = 0,
    public dir: V2D = V2D.rad2dir(0)
  ) {
    if (dir === null) {
      this.dir = V2D.rad2dir(value)
    }
  }
}
