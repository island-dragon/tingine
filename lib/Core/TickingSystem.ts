import { System } from './System'

export default abstract class TickingSystem extends System {
  protected tickCurrentMS: number = 0
  protected tickEveryMS: number = 16

  public tick(dt: number, ts: number) {
    this.tickCurrentMS += dt

    if (this.tickCurrentMS > this.tickEveryMS) {
      this.tickCurrentMS = 0
      this.update(dt, ts)
    }

    return true
  }

  public update(_dt: number, _ts: number): void { /* Nothing to do */ }
}
