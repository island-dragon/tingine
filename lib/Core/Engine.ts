import GraphicsEngine from '../Extras/Graphics/GraphicsEngine'
import { logger } from '../Extras/Logger'
import { System } from './System'

export class Engine {
  public graphics!: GraphicsEngine
  public masterEntity!: number
  public cleanUpEntities!: number[]

  constructor(
    private entities: any[] = [],
    private updateSystems: any[] = [],
    private renderSystems: any[] = [],
    private ts: number = 0,
  ) {
    this.start()
  }

  public setGraphicsEngine(graphics: GraphicsEngine) {
    this.graphics = graphics
  }

  public start(): void {
    logger.info('Starting engine')

    this.masterEntity = this.addEntity([])
    this.cleanUpEntities = []

    if (typeof window === 'undefined') {
      setImmediate(() => {
        const ts = process.hrtime()
        return this.tick(ts[0] * 1000 + ts[1] / 1000000)
      })
      return
    }
    window.requestAnimationFrame(ts => this.tick(ts))
  }

  public addUpdateSystem(system: any) {
    if (system.prototype instanceof System === false) {
      throw new Error(`Unable to add system ${system.name}. It does not extend System`)
    }

    const sys = new system(system.name.toString(), this)
    sys.init()
    this.updateSystems.push(sys)
    return sys
  }

  public getUpdateSystem(system: any) {
    return this.updateSystems.find((c: any) => c.constructor === system)
  }

  public addRenderSystem(system: any) {
    if (system.prototype instanceof System === false) {
      throw new Error(`Unable to add system ${system.name}. It does not extend System`)
    }

    const sys = new system(system.name.toString(), this)
    this.renderSystems.push(sys)
    return sys
  }

  public getRenderSystem(system: any) {
    return this.renderSystems.find((c: any) => c.constructor === system)
  }

  public prependRenderSystem(system: any) {
    if (system.prototype instanceof System === false) {
      throw new Error(`Unable to add system ${system.name}. It does not extend System`)
    }

    this.renderSystems.unshift(new system(system.name.toString(), this))
  }

  public removeUpdateSystem(system: any) {
    if (system.prototype instanceof System === false) {
      throw new Error(`Unable to remove system ${system.name}. It does not extend System`)
    }

    this.updateSystems = this.updateSystems.filter(sys => sys.constructor !== system)
  }

  public removeRenderSystem(system: any) {
    if (system.prototype instanceof System === false) {
      throw new Error(`Unable to remove system ${system.name}. It does not extend System`)
    }

    this.renderSystems = this.renderSystems.filter(sys => sys.constructor !== system)
  }

  public entityExists(entity: number) {
    return this.entities[entity] !== undefined
  }

  public addEntity(components: any[]) {
    const id = this.entities.length
    this.entities.push(components)
    return id
  }

  public removeEntity(entityId: number) {
    if (entityId === this.masterEntity) return

    this.cleanUpEntities.push(entityId)
  }

  public removeAllEntities() {
    this.entities.forEach((v, i) => {
      if (i === this.masterEntity || v === undefined) return
      this.cleanUpEntities.push(i)
    })
  }

  public getEntities(components: any[]) {
    return this.entities
      .map((val, index) => ({ index, val }))
      .filter(({ index, val }) =>
        val !== undefined
        && components.every(
          c => this.getComponent(index, c)
        )
      ).map(({ index }) => index)
  }

  public getEntitiesComponents(components: any[], forbiddenComponents: any[] = []): any[] {
    return this.entities.reduce((prev, cur, i) => {
      if (!cur || cur.length < components.length) { return prev }

      if (
        forbiddenComponents.length > 0
        && cur.find((c: any) => forbiddenComponents.find(fc => c.constructor === fc))
      ) {
        return prev
      }

      const entityComponents = components.map(c => cur.find((ec: any) => ec.constructor === c)).filter(c => c)

      if (entityComponents.length < components.length) { return prev }

      prev.push([entityComponents, i])
      return prev
    }, [])
  }

  public getComponents(entityId: number, components: any[] = []) {
    return this.entities[entityId].filter((c: any) => components.length === 0 ? true : components.find(component => c instanceof component))
  }

  public getComponent(entityId: number, component: any) {
    return this.entities[entityId] && this.entities[entityId].find((c: any) => c instanceof component)
  }

  public hasComponent(entityId: number, component: any) {
    return this.entities[entityId].find((c: any) => c instanceof component)
  }

  public addComponent(entityId: number, component: any) {
    this.entities[entityId].push(component)
    return entityId
  }

  public addComponents(entityId: number, components: any[]) {
    this.entities[entityId] = this.entities[entityId].concat(components)
    return entityId
  }

  public removeComponent(entityId: number, component: any) {
    this.entities[entityId] = this.entities[entityId].filter((c: any) => c.constructor !== component)
    return entityId
  }

  private tick(timeStamp: number) {
    const dt = timeStamp - this.ts

    this.updateTick(dt, timeStamp)
    this.renderTick(dt, timeStamp)
    this.cleanUpTick()

    this.ts = timeStamp

    if (typeof window === 'undefined') {
      setImmediate(() => {
        const ts = process.hrtime()
        return this.tick(ts[0] * 1000 + ts[1] / 1000000)
      })

      return
    }
    window.requestAnimationFrame(ts => this.tick(ts))
  }

  private updateTick(dt: number, ts: number) {
    this.updateSystems.forEach(sys => sys.tick(dt, ts))
  }
  private renderTick(dt: number, ts: number) {
    this.renderSystems.forEach(sys => sys.tick(dt, ts))
  }

  private cleanUpTick() {
    this.cleanUpEntities.forEach(e => {
      this.entities[e] = undefined
    })

    this.cleanUpEntities = []
  }
}
