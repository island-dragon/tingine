import { logger } from '../Extras/Logger'
import { Engine } from './Engine'

export abstract class System {
  protected tickCurrentMS: number = 0
  protected tickEveryMS: number = 16

  /**
   * List of components that an entity must have in order to qualify
   */
  protected components: any[] = []

  /**
   * List of components that an entity must NOT have in order to qualify
   */
  protected forbiddenComponents: any[] = []

  constructor(
    protected name: string,
    protected engine: Engine
  ) {
    logger.debug(`Init ${this.name}`)
    this.init()
  }

  public tick(dt: number, ts: number): boolean {
    this.tickCurrentMS += dt

    if (this.tickCurrentMS < this.tickEveryMS) {
      return false
    }

    this.getEntities(this.engine.getEntitiesComponents(this.components, this.forbiddenComponents))
      .forEach((componentsWithEntities: any[]) =>
        this.update(this.tickCurrentMS, ts, componentsWithEntities[1], ...componentsWithEntities[0])
      )
    this.tickCurrentMS = 0

    return true
  }

  public getEntities(entities: any[]): any[] {
    return entities
  }

  public init() { /* Nothing to do */ }

  // @ts-ignore
  public update(dt: number, ts: number, entity: number, ...args: any[]): void { /* Nothing to do */ }
}
