/**
 * @ignore
 */
const c = console

export enum LogLevel {
  Error,
  Warn,
  Info,
  Debug,
  Trace,
}

/**
 * @ignore
 */
let logLevel: LogLevel = LogLevel.Warn

export default class Logger {
  public SetLevel = (level: LogLevel) =>
    logLevel = level
  public debug = (...args: any[]) =>
    logLevel >= LogLevel.Debug && c.debug(...args)
  public error = (...args: any[]) =>
    logLevel >= LogLevel.Error && c.error(...args)
  public info = (...args: any[]) =>
    logLevel >= LogLevel.Info && c.info(...args)
  public log = (...args: any[]) =>
    c.log(...args)
  public trace = (...args: any[]) =>
    logLevel >= LogLevel.Trace && c.trace(...args)
  public warn = (...args: any[]) =>
    logLevel >= LogLevel.Warn && c.warn(...args)
}

export const logger = new Logger()
