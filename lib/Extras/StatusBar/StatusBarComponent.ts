export class StatusBarComponent {
  constructor(
    public fps: number,
    public lastDT: number,
    public dom: HTMLDivElement,
  ) { }
}
