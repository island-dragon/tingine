import { System } from '../../Core/System'
import { StatusBarComponent } from './StatusBarComponent'

export class StatusBarUpdateSystem extends System {
  public components = [StatusBarComponent]

  public times: number[] = []

  public init() {
    this.times = []
  }

  public update(dt: number, ts: number, _entity: number, statusBar: StatusBarComponent) {
    this.times.push(ts)
    const secondsToKeep = ts - 10000
    this.times = this.times.filter(t => t > secondsToKeep)
    statusBar.fps = this.times.length / 10
    statusBar.lastDT = dt
  }
}
