import { System } from '../../Core/System'
import { StatusBarComponent } from './StatusBarComponent'

export class StatusBarRenderSystem extends System {
  public components = [StatusBarComponent]

  public lastFPS: number = 0
  public statusBar!: HTMLDivElement

  public init() {
    this.lastFPS = 0
  }

  public update(_dt: number, _ts: number, _entity: number, statusBar: StatusBarComponent) {
    const fps = statusBar.fps
    if (fps === this.lastFPS) {
      return
    }
    statusBar.dom.innerText = `fps: ${fps.toFixed(2)}`
    this.lastFPS = fps
  }
}
