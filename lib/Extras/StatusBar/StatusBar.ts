import { Engine } from '../../Core/Engine'
import { logger } from '../Logger'
import { StatusBarComponent } from './StatusBarComponent'
import { StatusBarRenderSystem } from './StatusBarRenderSystem'
import { StatusBarUpdateSystem } from './StatusBarUpdateSystem'

export default class StatusBar {
  constructor(
    public engine: Engine,
    public fpsCounter = true,
    public lastTickDT = true,
    public dom: HTMLDivElement | null = null,
  ) {
    logger.info('Starting Status Bar')

    if (dom === null) {
      dom = document.createElement('div')
      document.body.prepend(dom)
    }
    this.dom = dom

    engine.addUpdateSystem(StatusBarUpdateSystem)
    engine.addRenderSystem(StatusBarRenderSystem)
    engine.addComponent(engine.masterEntity, new StatusBarComponent(0, 0, dom))
  }
}
