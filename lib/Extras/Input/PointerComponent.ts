import { Rect } from '../Graphics/Rect'

export class PointerComponent {
  constructor(
    public leftDownHandlers: Function[] = [],
    public leftHoldHandlers: Function[] = [],
    public leftUpHandlers: Function[] = [],
    public rect?: Rect
  ) { }
}
