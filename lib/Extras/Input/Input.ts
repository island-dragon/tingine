import { Engine } from '../../Core/Engine'
import { logger } from '../Logger'
import { InputComponent } from './InputComponent'
import { InputSystem } from './InputSystem'
import { PointerSystem } from './PointerSystem'

type Listener = (e: any) => void

interface IListeners {
  [key: string]: Handler[]
}

type Handler = (e: any) => boolean

export default class Input {
  // Implementing touch? Look at
  // - https://www.davrous.com/2015/08/10/unifying-touch-and-mouse-how-pointer-events-will-make-cross-browsers-touch-support-easy/
  // - https://developer.mozilla.org/en-US/docs/Games/Techniques/Control_mechanisms/Mobile_touch

  protected events: { [k: string]: Listener } = {
    click: () => true,
    load: () => true,
    mousedown: () => true,
    mousemove: () => true,
    mouseup: () => true,
    pointerdown: () => true,
    pointermove: () => true,
    pointerup: () => true,
  }

  protected listeners!: IListeners

  public component!: InputComponent

  constructor(
    public engine: Engine,
    private dom: HTMLCanvasElement
  ) {
    this.start()
  }

  public start() {
    logger.info('Starting Input')

    this.listeners = {}

    Object.keys(this.events).forEach(k => {
      this.events[k] = this.createHandler(k)
      this.listeners[k] = []
      this.dom.addEventListener(k, this.events[k])
    })

    this.component = new InputComponent(this)
    this.engine.addComponent(
      this.engine.masterEntity,
      this.component
    )

    this.engine.addUpdateSystem(InputSystem)
    this.engine.addUpdateSystem(PointerSystem)
  }

  public register(k: string, handler: any) {
    this.listeners[k].push(handler)
  }

  public deRegister(k: string, handler: any) {
    this.listeners[k] = this.listeners[k].filter(h => h !== handler)
  }

  public handle(e: any, listeners: Handler[]) {
    for (const h of listeners) {
      if (!h) { continue }
      if (h(e) === false) { break }
    }
  }

  public createHandler = (k: string) =>
    (e: any) => this.handle(e, this.listeners[k])
}
