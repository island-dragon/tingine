import { System } from '../../Core/System'
import Input from './Input'
import { InputComponent } from './InputComponent'

interface IState {
  x: number,
  y: number,
  left: boolean,
}

export class InputSystem extends System {
  public components = [InputComponent]
  public input!: Input
  public state!: IState

  public init() {
    this.state = {
      x: 0,
      y: 0,
      left: false,
    }

    this.input = (this.engine.getComponent(
      this.engine.getEntities([InputComponent])[0]
      , InputComponent
    ) as InputComponent).input

    this.input.register('pointerdown', (e: PointerEvent) => {
      if (e.pressure > 0) {
        this.state.left = true
        this.state.x = e.x
        this.state.y = e.y
      }
    })

    this.input.register('pointermove', (e: PointerEvent) => {
      if (e.pressure > 0) {
        this.state.left = true
        this.state.x = e.x
        this.state.y = e.y
      }
    })

    this.input.register('pointerup', (e: PointerEvent) => {
      this.state.left = false
      this.state.x = e.x
      this.state.y = e.y
    })
  }

  public update(
    _dt: number,
    _ts: number,
    _entity: number,
    input: InputComponent
  ) {
    input.x = this.state.x
    input.y = this.state.y
    input.leftUp = (input.leftDown || input.leftHold) && !this.state.left
    input.leftDown = !input.leftDown && this.state.left && !input.leftHold
    input.leftHold = !input.leftDown && this.state.left
  }
}
