import Input from './Input'

export class InputComponent {
  constructor(
    public input: Input,
    public x: number = 0,
    public y: number = 0,
    public leftDown: boolean = false,
    public leftHold: boolean = false,
    public leftUp: boolean = false,
    public pointerDown: boolean = false,
    public pointerHold: boolean = false,
    public pointerUp: boolean = false,
  ) { }
}
