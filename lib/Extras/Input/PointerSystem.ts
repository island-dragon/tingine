import { System } from '../../Core/System'
import { PointerComponent } from './PointerComponent'
import { InputComponent } from './InputComponent'
import { Position2D, Parent } from '../../Components'
import V2D from '../Maths/V2D'

export class PointerSystem extends System {
  public components = [PointerComponent]
  public inputComponent!: InputComponent

  public init() {
    this.inputComponent = this.engine.getComponent(
      this.engine.getEntities([InputComponent])[0]
      , InputComponent
    ) as InputComponent
  }

  getEntities(entities: any[]): any[] {
    return entities.reverse()
  }

  public update(
    _dt: number,
    _ts: number,
    entity: number,
    pointer: PointerComponent
  ) {
    let v = new V2D()
    v.clone(this.engine.getComponent(entity, Position2D).point)

    const parentComponent = this.engine.getComponent(entity, Parent) as Parent
    if (parentComponent) {
      v.set(v.x + parentComponent.globalPoint.point.x, v.y + parentComponent.globalPoint.point.y)
    }

    let act = false
    if (pointer.rect) {
      const w = this.inputComponent.x - v.x - pointer.rect.x
      const h = this.inputComponent.y - v.y - pointer.rect.y
      if (w > 0 && w < pointer.rect.w && h > 0 && h < pointer.rect.h) {
        act = true
      }
    } else {
      if (V2D.distance(v, new V2D(this.inputComponent.x, this.inputComponent.y)) < 5) {
        act = true
      }
    }

    if (act && this.inputComponent.leftDown && pointer.leftDownHandlers.length) {
      pointer.leftDownHandlers.forEach(f => f(entity))
    }

    if (act && this.inputComponent.leftHold && pointer.leftHoldHandlers.length) {
      pointer.leftHoldHandlers.forEach(f => f(entity))
    }

    if (act && this.inputComponent.leftUp && pointer.leftUpHandlers.length) {
      pointer.leftUpHandlers.forEach(f => f(entity))
    }
  }
}
