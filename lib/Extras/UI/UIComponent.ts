import { UIFunc } from './UI'

export class UIComponent {
  constructor(
    public func: UIFunc,
    public drawCalls: any[] = [],
  ) { }
}
