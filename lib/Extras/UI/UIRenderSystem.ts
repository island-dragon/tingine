import { System } from '../../Core/System'
import { UIComponent } from './UIComponent'

export default class UIRenderSystem extends System {
  public components = [UIComponent]

  public lastFPS: number = 0
  public statusBar!: HTMLDivElement

  public init() {
    this.lastFPS = 0
  }

  public update(_dt: number, _ts: number, _entity: number, ui: UIComponent) {
    let drawCall: () => void
    let args: []
    for ([drawCall, args] of ui.drawCalls) {
      drawCall.apply(this.engine.graphics, args)
    }
    ui.drawCalls = []
  }
}
