import { System } from '../../Core/System'
import GraphicsEngine from '../Graphics/GraphicsEngine'
import { UIComponent } from './UIComponent'

export class UISystem extends System {
  public components = [UIComponent]
  public geProxy!: GraphicsEngine
  public handler!: ProxyHandler<GraphicsEngine>
  public uiComponent!: UIComponent

  public intercept(...args: any[]) {
    // @ts-ignore
    this.uiComponent.drawCalls.push([this.method, args])
  }

  public init() {
    const uiSystem = this

    this.uiComponent = this.engine.getComponent(this.engine.masterEntity, UIComponent) as UIComponent

    this.handler = {
      get(target: GraphicsEngine, p: string) {
        // @ts-ignore
        const method = target[p]
        return uiSystem.intercept.bind({ method, uiComponent: uiSystem.uiComponent})
      },
    }
    this.geProxy = new Proxy(this.engine.graphics, this.handler)
  }

  public update(_dt: number,
    _ts: number,
    _entity: number,
    uic: UIComponent
  ) {
    if (uic.func) {
      uic.func()
    }
  }
}
