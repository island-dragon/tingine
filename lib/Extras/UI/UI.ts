import { Rect } from '../../Extras/Graphics/Rect'
import { AlignmentType } from '../Graphics/AlignmentType'
import V2D from '../Maths/V2D'
import { SpriteSheet } from '../Graphics/SpriteSheet'
import { Engine } from '../../Core'
import { InputComponent } from '../Input'
import UIRenderSystem from './UIRenderSystem'
import { UISystem } from '.'

export type UIFunc = () => void

interface IUIComponent {
  bgColor: string
  bgColorHL: string
  bgSprite: string
  font: string
  fontColor: string
  fontColorHL: string
  fontSize: number
}

interface ISkin {
  button: IUIComponent
  title: IUIComponent
  panel: IUIComponent
  checkbox: {
    sprite: string
    spriteOn: string
  }
  clearBg: string
  font: string
  fontColor: string
  fontSize: number
}

export interface IUIOptions {
  alignment?: AlignmentType
  anchor?: V2D
  fgColor?: string
  bgColor?: string
  bgSprite?: string
}

export interface IUITextOptions extends IUIOptions {
  font?: string
  fontSize?: number
}

export class UI {
  public spriteSheet: SpriteSheet
  public skin: ISkin
  public ctx: CanvasRenderingContext2D
  public input: InputComponent
  public fontSize: number
  public engine: Engine

  constructor(engine: Engine, spritesheet: SpriteSheet) {
    this.engine = engine
    this.ctx = engine.graphics.ctx
    this.input = this.engine.getComponent(this.engine.masterEntity, InputComponent) as InputComponent
    this.fontSize = 16

    this.spriteSheet = spritesheet
    this.skin = {
      button: {
        bgColor: '#A3703A',
        bgColorHL: '#A3703A',
        bgSprite: 'panel_woodDetail.png',
        font: 'Roboto',
        fontColor: '#FFF1D2',
        fontColorHL: '#FFFFFF',
        fontSize: this.fontSize,
      },
      title: {
        bgColor: '',
        bgColorHL: '',
        bgSprite: '',
        font: 'Roboto',
        fontColor: '#B47B41',
        fontColorHL: '',
        fontSize: this.fontSize * 2,
      },
      panel: {
        bgColor: '#FFF1D2DD',
        bgColorHL: '',
        bgSprite: 'panel_woodDetail_blank.png',
        font: '',
        fontColor: '',
        fontColorHL: '',
        fontSize: 0,
      },
      checkbox: {
        sprite: 'button_woodPaperBlank.png',
        spriteOn: 'button_woodPaperCircle.png',
      },
      clearBg: '#222222',
      font: 'Roboto',
      fontColor: '#FFF1D2',
      fontSize: this.fontSize,
    }

    engine.addUpdateSystem(UISystem)
    engine.addRenderSystem(UIRenderSystem)
  }

  public rawSprite = (
    source: Rect,
    dest: Rect,
  ) => {
    this.ctx.drawImage(
      this.spriteSheet.img,
      source.x,
      source.y,
      source.w,
      source.h,
      dest.x,
      dest.y,
      dest.w,
      dest.h,
    )
  }

  public drawFrame(
    rect: Rect,
    options: IUIOptions = {},
    // withClose: boolean = false,
  ) {
    if (options.alignment !== undefined) {
      rect = Rect.alignRect(rect, options.alignment)
    }

    let anchor = new V2D()
    if (options.anchor !== undefined) {
      anchor = options.anchor
    }

    rect.x += anchor.x
    rect.y += anchor.y

    const sprite = this.spriteSheet.data.frames[options.bgSprite || this.skin.panel.bgSprite]
    this.ctx.fillStyle = options.bgColor || this.skin.panel.bgColor
    this.ctx.fillRect(rect.x + 10, rect.y + 10, rect.w - 20, rect.h - 20)

    // TL
    this.rawSprite(
      new Rect(sprite.frame.x, sprite.frame.y, 22, 22),
      new Rect(rect.x, rect.y, 22, 22),
    )

    // TR
    this.rawSprite(
      new Rect(sprite.frame.x + 42, sprite.frame.y, 22, 22),
      new Rect(rect.x + rect.w - 22, rect.y, 22, 22),
    )

    // TL
    this.rawSprite(
      new Rect(sprite.frame.x, sprite.frame.y + 42, 22, 22),
      new Rect(rect.x, rect.y + rect.h - 22, 22, 22),
    )

    // BR
    this.rawSprite(
      new Rect(sprite.frame.x + 42, sprite.frame.y + 42, 22, 22),
      new Rect(rect.x + rect.w - 22, rect.y + rect.h - 22, 22, 22),
    )

    // T
    this.rawSprite(
      new Rect(sprite.frame.x + 22, sprite.frame.y, 1, 22),
      new Rect(rect.x + 22, rect.y, rect.w - 44, 22),
    )

    // B
    this.rawSprite(
      new Rect(sprite.frame.x + 22, sprite.frame.y + 42, 1, 22),
      new Rect(rect.x + 22, rect.y + rect.h - 22, rect.w - 44, 22),
    )

    // L
    this.rawSprite(
      new Rect(sprite.frame.x, sprite.frame.y + 22, 22, 1),
      new Rect(rect.x, rect.y + 22, 22, rect.h - 44),
    )

    // R
    this.rawSprite(
      new Rect(sprite.frame.x + 42, sprite.frame.y + 22, 22, 1),
      new Rect(rect.x + rect.w - 22, rect.y + 22, 22, rect.h - 44),
    )
  }

  public setFontStyle = ({ fontSize, font, fgColor }: IUITextOptions) => {
    this.fontSize = fontSize || this.skin.fontSize
    this.ctx.font = `${this.fontSize}px ${font || this.skin.font}`
    this.ctx.fillStyle = fgColor || this.skin.fontColor
  }

  public text = (
    label: string,
    x: number,
    y: number,
    options: IUITextOptions = {}
  ) => {
    this.setFontStyle(options)

    let rect = new Rect(x, y)
    if (options.alignment !== undefined) {
      rect.w = this.ctx.measureText(label).width
      rect.h = this.fontSize * 0.9
      rect = Rect.alignRect(rect, options.alignment)
    }

    this.ctx.fillText(label, rect.x, rect.y)

    this.setFontStyle({})
  }

  public title = (
    label: string,
    x: number,
    y: number,
    options: IUITextOptions = {}
  ) => {
    this.text(
      label,
      x,
      y,
      {
        ...{
          alignment: AlignmentType.C,
          fontSize: this.skin.title.fontSize,
          fgColor: this.skin.title.fontColor
        },
        ...options
      }
    )
  }

  public button = (label: string, rect: Rect, alignment: AlignmentType = AlignmentType.C) => {
    if (!rect.w) {
      rect.w = this.ctx.measureText(label).width + 40
    }
    if (!rect.h) {
      rect.h = 44
    }

    rect = Rect.alignRect(rect, alignment)

    let fgColor = ''

    this.engine

    if (V2D.pointWithinRect(new V2D(this.input.x, this.input.y), rect)) {
      if (this.input.leftDown) {
        return true
      }

      fgColor = this.skin.button.fontColorHL
    }

    this.drawFrame(rect, { ...this.skin.button })
    this.text(label, rect.x + rect.w / 2, rect.y + rect.h / 2, { ...this.skin.button, fgColor, alignment: AlignmentType.C })

    return false
  }

  public checkbox = (label: string, state: any, rect: Rect, value: any = true) => {
    rect.w = 24
    rect.h = 24
    const uiRect = new Rect(rect.x, rect.y, rect.w + 5 + this.ctx.measureText(label).width, rect.h)

    const on = typeof state === 'boolean' ? state : state === value

    const sprite = this.spriteSheet.data.frames[on ? this.skin.checkbox.spriteOn : this.skin.checkbox.sprite]
    this.rawSprite(
      { ...sprite.frame },
      rect,
    )

    this.text(label, rect.x + rect.w + 5, rect.y + 5)

    return (V2D.pointWithinRect(new V2D(this.input.x, this.input.y), uiRect) && this.input.leftDown)
      ? value === true ? !state : value
      : state
  }
}
