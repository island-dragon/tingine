import { Rect } from "./Rect";
import { AlignmentType } from "./AlignmentType";
import { SpriteSheet } from "./SpriteSheet";

export class BaseData {
  public rotation: number = 0
  public parentX: number = 0
  public parentY: number = 0
}

export class SpriteAnimationData extends BaseData {
  constructor(
    public data: {
      spriteSheet: SpriteSheet
    },
  ) {
    super()
  }
}

export class RawSpriteData extends BaseData {
  constructor(
    public src: Rect,
    public dst: Rect,
    public img: CanvasImageSource,
  ) { super() }
}

export class SpriteData extends BaseData {
  constructor(
    public data: {
      x?: number,
      y?: number,
      img: CanvasImageSource
    },
  ) {
    super()

    this.data = {
      x: 0,
      y: 0,
      ...data
    }
  }
}

export class TextData extends BaseData {
  constructor(
    public data: {
      label: string,
      x?: number,
      y?: number,
      color?: string,
      fontSize?: number,
      fontDecorations: string,
      fontFace?: string,
      alignment?: AlignmentType,
    }
  ) {
    super()

    this.data = {
      x: 0,
      y: 0,
      alignment: AlignmentType.C,
      color: 'white',
      fontFace: 'verdana',
      fontSize: 10,
      ...data
    }
  }
}

export class CircleData extends BaseData {
  constructor(
    public data: {
      x?: number,
      y?: number,
      radius?: number,
      color?: string,
    },
  ) {
    super()

    this.data = {
      x: 0,
      y: 0,
      radius: 50,
      color: 'red',
      ...data
    }
  }
}

export class RectangleData extends BaseData {
  constructor(
    public data: {
      dst: Rect
      color?: string
      line?: string
    },
  ) {
    super()

    this.data = {
      color: 'red',
      line: '',
      ...data
    }

    this.data.dst.x -= this.data.dst.w / 2
    this.data.dst.y -= this.data.dst.h / 2

  }
}

export class SpriteByNameData extends BaseData {
  constructor(
    public data: {
      x?: number,
      y?: number,
      spriteSheet: SpriteSheet
      name: string
      overhang?: boolean
    },
  ) {
    super()

    this.data = {
      overhang: false,
      ...data
    }

    const ss = this.data.spriteSheet.getDataFor(this.data.name)
    if (ss) {
      this.data.x = this.data.x ?? 0 - ss.w / 2
      this.data.y = this.data.y ?? 0 - ss.h / 2
    }
  }
}