import { System } from '../../Core/System'
import SpriteSheetAnimationComponent from './SpriteSheetAnimationComponent'

export default class SpriteSheetAnimationSystem extends System {
  public components = [SpriteSheetAnimationComponent]

  public update(dt: number, _ts: number, _entity: number, spriteSheet: SpriteSheetAnimationComponent) {
    spriteSheet.progress += dt
    if (spriteSheet.progress > spriteSheet.speed) {
      spriteSheet.progress = 0
      spriteSheet.currentSprite = (spriteSheet.currentSprite + 1) % spriteSheet.animation.length
    }
  }
}
