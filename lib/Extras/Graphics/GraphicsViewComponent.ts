import { SpriteAnimationData, RawSpriteData, CircleData, RectangleData, SpriteByNameData, SpriteData, TextData } from './components'

export default class GraphicsViewComponent {
  constructor(
    public data
      : SpriteAnimationData
      | RawSpriteData
      | CircleData
      | RectangleData
      | SpriteByNameData
      | SpriteData
      | TextData,
    public depth: number = 0,
  ) { }
}
