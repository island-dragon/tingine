import { AlignmentType } from "./AlignmentType"
import V2D from "../Maths/V2D"

export class Rect {
  static calculateAlignmentOffset(aligment: AlignmentType, w: number, h: number) {
    let offset = new V2D()

    switch (aligment) {
      case AlignmentType.TL:
        offset.x = 0
        offset.y = 0
        break
      case AlignmentType.L:
        offset.x = 0
        offset.y = -h / 2
        break
      case AlignmentType.BL:
        offset.x = 0
        offset.y = -h
        break
      case AlignmentType.T:
        offset.x = -w / 2
        offset.y = 0
        break
      case AlignmentType.C:
        offset.x = -w / 2
        offset.y = -h / 2
        break
      case AlignmentType.B:
        offset.x = -w / 2
        offset.y = -h
        break
      case AlignmentType.TR:
        offset.x = -w
        offset.y = 0
        break
      case AlignmentType.R:
        offset.x = -w
        offset.y = -h / 2
        break
      case AlignmentType.BR:
        offset.x = -w
        offset.y = -h
        break
    }

    return offset
  }
  public static sumV2 = (rect: Rect, p: V2D) => new Rect(rect.x + p.x, rect.y + p.y, rect.w, rect.h)

  public static alignRect(rect: Rect, align: AlignmentType): Rect {
    const map = {
      [AlignmentType.TL]: [0, 0],
      [AlignmentType.L]: [0, rect.h / 2],
      [AlignmentType.BL]: [0, rect.h],

      [AlignmentType.T]: [rect.w / 2, 0],
      [AlignmentType.C]: [rect.w / 2, rect.h / 2],
      [AlignmentType.B]: [rect.w / 2, rect.h],

      [AlignmentType.TR]: [rect.w, 0],
      [AlignmentType.R]: [rect.w, rect.h / 2],
      [AlignmentType.BR]: [rect.w, rect.h],
    }

    return new Rect(
      rect.x - map[align][0],
      rect.y - map[align][1],
      rect.w,
      rect.h
    )
  }

  public static overlap(a: Rect, b: Rect): boolean {
    if (a.x >= b.x + b.w) return false
    if (b.x >= a.x + a.w) return false

    if (a.y >= b.y + b.h) return false
    if (b.y >= a.y + a.h) return false

    return true
  }

  constructor(
    public x: number = 0,
    public y: number = 0,
    public w: number = 0,
    public h: number = 0,
  ) { }
}
