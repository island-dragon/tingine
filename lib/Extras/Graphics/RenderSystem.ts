import { Parent } from '../../Components/Parent'
import { Position2D } from '../../Components/Position2D'
import { Rotation2D } from '../../Components/Rotation2D'
import { FlipFlop2D } from '../../Components/FlipFlop2D'
import { System } from '../../Core/System'
import GraphicsViewComponent from './GraphicsViewComponent'
import SpriteSheetAnimationComponent from './SpriteSheetAnimationComponent'
import { RawSpriteData, TextData, SpriteAnimationData, SpriteData, CircleData, RectangleData, SpriteByNameData, BaseData } from './components'

export default class RenderSystem extends System {
  public components = [Position2D, GraphicsViewComponent]

  /**
   * It would be good to have a way to ask for N sets of entities with different
   * sets of components. In this particular case:
   *  - SetA [Graphics_ViewComponent] used for rendering objects
   *  - SetB [Graphics_Component] used triggering render calls
   *
   * For now i just cache it on the system itself
   */

  public tick(dt: number, ts: number) {
    this.engine.getEntitiesComponents(this.components, this.forbiddenComponents)
      .sort((a: any[], b: any[]) => {
        if (a[0][1].depth < b[0][1].depth) {
          return -1
        } else if (a[0][1].depth > b[0][1].depth) {
          return 1
        }

        return 0
      })
      .forEach((componentsWithEntities: any[]) =>
        this.update(dt, ts, componentsWithEntities[1], componentsWithEntities[0][0], componentsWithEntities[0][1])
      )

    return true
  }

  public update(_dt: number, _ts: number, entity: number, position: Position2D, view: GraphicsViewComponent) {
    const parent = this.engine.getComponent(entity, Parent) as Parent
    let parentX = 0
    let parentY = 0
    if (parent) {
      parentX = parent.globalPoint.point.x
      parentY = parent.globalPoint.point.y
    }

    const rotation = this.engine.getComponent(entity, Rotation2D) as Rotation2D

    this.engine.graphics.ctx.save()
    this.engine.graphics.ctx.translate(
      position.point.x + parentX,
      position.point.y + parentY,
    )

    if (rotation) {
      const baseData = view.data as BaseData
      this.engine.graphics.ctx.rotate(rotation.value + baseData.rotation)
    }

    const flipflop = this.engine.getComponent(entity, FlipFlop2D) as FlipFlop2D

    if (flipflop) {
      this.engine.graphics.ctx.scale(
        flipflop.flip ? -1 : 1,
        flipflop.flop ? -1 : 1,
      )
    }

    if (view.data instanceof SpriteAnimationData) {
      const spriteSheet =
        this.engine.getComponent(entity, SpriteSheetAnimationComponent) as SpriteSheetAnimationComponent

      this.engine.graphics.drawSpriteFromSpriteSheetByName({
        parentX: 0,
        parentY: 0,
        rotation: 0,
        data: {
          spriteSheet: view.data.data.spriteSheet,
          name: spriteSheet.animation[spriteSheet.currentSprite]
        }
      })
    }

    if (view.data instanceof SpriteData)
      this.engine.graphics.drawSprite(view.data)

    if (view.data instanceof SpriteByNameData)
      this.engine.graphics.drawSpriteFromSpriteSheetByName(view.data)

    if (view.data instanceof RectangleData)
      this.engine.graphics.drawRectangle(view.data)

    if (view.data instanceof CircleData)
      this.engine.graphics.drawCircle(view.data)

    if (view.data instanceof RawSpriteData)
      this.engine.graphics.drawRawSprite(view.data)

    if (view.data instanceof TextData)
      this.engine.graphics.drawText(view.data)

    this.engine.graphics.ctx.restore()
  }
}
