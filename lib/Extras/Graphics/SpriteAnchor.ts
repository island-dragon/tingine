enum SpriteAnchor {
  None,
  TL,
  C,
  BC,
  Custom,
}

export default SpriteAnchor
