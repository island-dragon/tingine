export enum AlignmentType {
    TL,
    T,
    TR,
    L,
    C,
    R,
    BL,
    B,
    BR,
}
