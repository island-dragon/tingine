import SpriteAnchor from './SpriteAnchor'

export default class SpriteSheetComponent {
  constructor(
    public width: number,
    public spriteSize: number,
    public spriteAnchor: SpriteAnchor = SpriteAnchor.TL,
  ) { }
}
