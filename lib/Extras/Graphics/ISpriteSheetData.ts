export interface IFrameData {
  x: number
  y: number
  w: number
  h: number
}

export interface ISpriteSheetData {
  frames: {
    [k in string]: {
      frame: IFrameData
      rotated: boolean
      trimmed: boolean
      spriteSourceSize: IFrameData
      sourceSize: {
        w: number
        h: number
      }
    }
  }
  meta: {
    app: string
    version: string
    image: string
    format: string
    size: {
      w: number
      h: number
    }
    scale: string
    smartupdate: string
  }
}
