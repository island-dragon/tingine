import { Position2D } from '../../Components'
import V2D from '../Maths/V2D'
import GraphicsViewComponent from './GraphicsViewComponent'
import { RectangleData } from './components'
import { Rect } from './Rect'

export const rectangle = (
  x: number,
  y: number,
  width: number,
  height: number,
  color: string,
  rel?: V2D
) =>
  [
    new Position2D(x, y, rel),
    new GraphicsViewComponent(
      new RectangleData({
        dst: new Rect(x, y, width, height),
        color
      })
    )
  ]
