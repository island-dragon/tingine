enum ViewType {
  None,
  Circle,
  Rectangle,
  Sprite,
  SpriteAnimation,
  SpriteByName,
  Cube25D,
  RawSprite,
  Text
}

export default ViewType
