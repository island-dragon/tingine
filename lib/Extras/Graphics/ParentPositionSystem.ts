import { System } from '../../Core/System'
import { Parent } from './../../Components/Parent'
import { Position2D } from './../../Components/Position2D'

export default class ParentPositionSystem extends System {
  public components = [Position2D, Parent]

  public update(_dt: number, _ts: number, _entity: number, _position: Position2D, parent: Parent) {
    let x = 0
    let y = 0

    const parentParent = this.engine.getComponent(parent.value, Parent) as Parent
    if (parentParent) {
      x = parentParent.globalPoint.point.x
      y = parentParent.globalPoint.point.y
    }

    const parentPosition = this.engine.getComponent(parent.value, Position2D) as Position2D
    parent.globalPoint.point.x = parentPosition.point.x + x
    parent.globalPoint.point.y = parentPosition.point.y + y
  }
}
