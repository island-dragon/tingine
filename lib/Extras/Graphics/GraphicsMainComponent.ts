import GraphicsEngine from './GraphicsEngine'

export default class GraphicsMainComponent {
  constructor(
    public graphics: GraphicsEngine,
  ) { }
}
