import { System } from '../../Core/System'
import GraphicsMainComponent from './GraphicsMainComponent'

export default class ClearFrameRenderSystem extends System {
  public components = [GraphicsMainComponent]

  public update(_dt: number, _ts: number, _entity: number, main: GraphicsMainComponent) {
    main.graphics.clearScreen()
  }
}
