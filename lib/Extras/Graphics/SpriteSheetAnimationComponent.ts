import V2D from '../Maths/V2D'
import SpriteAnchor from './SpriteAnchor'

export default class SpriteSheetAnimationComponent {
  constructor(
    public animation: string[],
    public speed: number,
    public source: HTMLImageElement,
    public spriteAnchor: SpriteAnchor = SpriteAnchor.TL,
    public customSpriteAnchor: V2D = new V2D(),
    public currentSprite: number = 0,
    public progress: number = 0,
  ) { }
}
