import { IFrameData, ISpriteSheetData } from './ISpriteSheetData'

export class SpriteSheet {
  public img: HTMLImageElement

  constructor(src: string, public data: ISpriteSheetData) {
    this.img = new Image(data.meta.size.w, data.meta.size.h)
    this.img.src = src
  }

  public getDataFor(name: string): IFrameData | null {
    const data = this.data.frames[name]
    if (data) {
      return data.frame
    }

    return null
  }
}
