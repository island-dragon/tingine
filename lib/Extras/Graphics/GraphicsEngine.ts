import { Engine } from '../../Core/Engine'
import { logger } from '../Logger'
import ClearFrameRenderSystem from './ClearFrameRenderSystem'
import GraphicsMainComponent from './GraphicsMainComponent'
import ParentPositionSystem from './ParentPositionSystem'
import RenderSystem from './RenderSystem'
import SpriteSheetAnimationSystem from './SpriteSheetAnimationSystem'
import { AlignmentType } from './AlignmentType'
import V2D from '../Maths/V2D'
import { CircleData, RectangleData, SpriteData, RawSpriteData, TextData, SpriteByNameData } from './components'

export interface Anchors {
  LT: V2D
  LC: V2D
  LB: V2D
  CT: V2D
  CC: V2D
  CB: V2D
  RT: V2D
  RC: V2D
  RB: V2D
}

export default class GraphicsEngine {
  public canvas: HTMLCanvasElement
  public ctx: CanvasRenderingContext2D
  public resizeLambda: (e: UIEvent) => void
  public portrait: boolean = false
  public anchors: Anchors = {
    LT: new V2D(),
    LC: new V2D(),
    LB: new V2D(),
    CT: new V2D(),
    CC: new V2D(),
    CB: new V2D(),
    RT: new V2D(),
    RC: new V2D(),
    RB: new V2D(),
  }

  constructor(public engine: Engine, public minWidth = 1280, public clearColor = 'black') {
    this.canvas = document.createElement('canvas')
    document.body.appendChild(this.canvas)

    this.setCanvasDimensions()
    this.resizeLambda = _ => this.setCanvasDimensions()
    window.addEventListener('resize', this.resizeLambda)

    const ctx = this.canvas.getContext('2d')
    if (ctx === null) {
      throw new Error('Unable to find context.')
    }
    this.ctx = ctx

    this.engine.addComponent(
      this.engine.masterEntity,
      new GraphicsMainComponent(this)
    )

    this.engine.setGraphicsEngine(this)

    engine.addRenderSystem(SpriteSheetAnimationSystem)
    engine.addRenderSystem(ClearFrameRenderSystem)
    engine.addRenderSystem(ParentPositionSystem)
    engine.addRenderSystem(RenderSystem)
  }

  public clearScreen() {
    // this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
    this.ctx.fillStyle = this.clearColor
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height)
  }

  public drawCircle({
    parentX, parentY,
    data: { x = 0, y = 0, radius = 10, color = 'red' }
  }: CircleData) {
    this.ctx.fillStyle = color
    this.ctx.beginPath()
    this.ctx.arc(x + parentX, y + parentY, radius, 0, 2 * Math.PI)
    this.ctx.fill()
  }

  public drawRectangle({
    parentX, parentY,
    data: { dst: { x, y, w, h }, color = 'red', line = '' }
  }: RectangleData) {
    this.ctx.fillStyle = color
    this.ctx.strokeStyle = line
    this.ctx.lineWidth = 1
    this.ctx.beginPath()
    this.ctx.rect(x + parentX, y + parentY, w, h)
    this.ctx.fill()
    this.ctx.stroke()
  }

  public drawSprite({
    parentX, parentY,
    data: { x = 0, y = 0, img }
  }: SpriteData) {
    this.ctx.drawImage(img, x + parentX, y + parentY)
  }

  public drawRawSprite({ src, dst, img, parentX, parentY }: RawSpriteData) {
    this.engine.graphics.ctx.drawImage(
      img,
      src.x,
      src.y,
      src.w,
      src.h,
      dst.x + parentX,
      dst.y + parentY,
      dst.w,
      dst.h
    )
  }

  public drawSpriteFromSpriteSheet(
    img: HTMLImageElement,
    x: number,
    y: number,
    index: number,
    spriteSize: number = 64,
    spriteSheetWidth: number = 1024
  ) {
    const sx = (index * spriteSize) % spriteSheetWidth
    const sy = Math.floor(index * spriteSize / spriteSheetWidth) * spriteSize
    this.ctx.drawImage(img, sx, sy, spriteSize, spriteSize, x, y, spriteSize, spriteSize)
  }

  public drawSpriteFromSpriteSheetByName({
    parentX, parentY,
    data: {
      spriteSheet, name, overhang
    }
  }: SpriteByNameData
  ) {
    const data = spriteSheet.getDataFor(name)
    if (!data) return

    const { x: sx = 0, y: sy = 0, w: sw = 0, h: sh = 0 } = data
    this.ctx.drawImage(
      spriteSheet.img,
      sx,
      sy,
      sw,
      sh,
      0 + parentX - sw / 2,
      0 + parentY - sh / 2,
      sw + (overhang ? 1 : 0),
      sh + (overhang ? 1 : 0)
    )
  }

  public drawText({
    data: {
      x = 0,
      y = 0,
      label,
      color = 'red',
      fontSize = 10,
      fontDecorations,
      fontFace = 'arial',
      alignment = AlignmentType.L
    }
  }: TextData) {
    const ctx = this.engine.graphics.ctx
    ctx.font = `${fontDecorations} ${fontSize}px ${fontFace}`
    ctx.fillStyle = color
    const text = label.split('\n')

    const oldTextAlign = ctx.textAlign
    const oldTextBaseline = ctx.textBaseline

    switch (alignment) {
      case AlignmentType.TL:
      case AlignmentType.L:
      case AlignmentType.BL:
        ctx.textAlign = 'left'
        break;
      case AlignmentType.T:
      case AlignmentType.C:
      case AlignmentType.B:
        ctx.textAlign = 'center'
        break;
      case AlignmentType.TR:
      case AlignmentType.R:
      case AlignmentType.BR:
        ctx.textAlign = 'right'
        break;
    }

    switch (alignment) {
      case AlignmentType.TL:
      case AlignmentType.T:
      case AlignmentType.TR:
        ctx.textBaseline = 'top'
        break;
      case AlignmentType.L:
      case AlignmentType.C:
      case AlignmentType.R:
        ctx.textBaseline = 'middle'
        break;
      case AlignmentType.BL:
      case AlignmentType.B:
      case AlignmentType.BR:
        ctx.textBaseline = 'bottom'
        break;
    }

    text.forEach((l: string, i: number) => {
      ctx.fillText(
        l,
        x,
        y + i * (fontSize + fontSize / 4),
        1024
      )
    })

    ctx.textAlign = oldTextAlign
    ctx.textBaseline = oldTextBaseline
  }

  public setCanvasDimensions() {
    const width = window.innerWidth
    const height = window.innerHeight - 4

    logger.info(`Reizing canvas to Width: ${width} Height: ${height}`)

    this.canvas.width = width
    this.canvas.height = height

    this.portrait = width < this.minWidth

    this.anchors.LC.y = height / 2
    this.anchors.LB.y = height
    this.anchors.CT.x = width / 2
    this.anchors.CC.set(width / 2, height / 2)
    this.anchors.CB.set(width / 2, height)
    this.anchors.RT.x = width
    this.anchors.RC.set(width, height / 2)
    this.anchors.RB.set(width, height)
  }
}
