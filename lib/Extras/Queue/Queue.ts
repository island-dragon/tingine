export class Queue<T> implements Iterable<T> {
  protected queue: T[] = []
  protected i: number = 0

  public enq = (item: T) => this.queue.push(item)
  public deq = () => this.queue.shift() as T
  public peek = () => this.queue[0]
  public clear = () => { this.queue = [] }
  public len = () => this.queue.length

  public next = (): IteratorResult<T> => ({
    done: this.i === this.queue.length,
    value: this.deq()
  })

  public [Symbol.iterator] = (): IterableIterator<T> => this
}
