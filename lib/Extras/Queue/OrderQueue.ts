import { Queue } from './Queue'

export class OrderQueueItem {
  constructor(
    public order: () => boolean,
    public done: () => boolean,
  ) { }
}

export const Q = new Queue<OrderQueueItem>()
