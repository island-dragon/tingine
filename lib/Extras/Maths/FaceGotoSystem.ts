import { Position2D } from '../../Components/Position2D'
import { Rotation2D } from '../../Components/Rotation2D'
import { System } from '../../Core/System'
import GoToComponent from './GoToComponent'
import GoToDoneComponent from './GoToDoneComponent'

export class FaceGotoSystem extends System {
  public components = [Position2D, Rotation2D, GoToComponent]
  public forbiddenComponents = [GoToDoneComponent]

  public update = (
    _dt: number,
    _ts: number,
    _entity: number,
    pos: Position2D,
    rot: Rotation2D,
    goto: GoToComponent,
  ) => {
    const targetPosition = this.engine.getComponent(goto.value, Position2D) as Position2D

    rot.dir = targetPosition.point.sub(pos.point)
    rot.value = Math.atan2(rot.dir.y, rot.dir.x) - Math.PI
  }
}
