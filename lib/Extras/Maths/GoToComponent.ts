export default class GoToComponent {
  constructor(
    public value: number,
    public speed: number,
    public useDone: boolean = false
  ) { }
}
