import { Position2D } from '../../Components/Position2D'
import { System } from '../../Core/System'
import GoToComponent from './GoToComponent'
import GoToDoneComponent from './GoToDoneComponent'

export default class GotoSystem extends System {
  public components = [Position2D, GoToComponent]
  public forbiddenComponents = [GoToDoneComponent]

  public update(dt: number, _ts: number, entity: number, position: Position2D, goto: GoToComponent) {
    const targetPosition = this.engine.getComponent(goto.value, Position2D) as Position2D

    const d = position.point.distanceTo(targetPosition.point)
    const s = goto.speed * dt
    if (d > s) {
      const move = position.point.sub(targetPosition.point).normalize().mul(s)

      position.point.x += move.x
      position.point.y += move.y
      return
    }

    position.point.x = targetPosition.point.x
    position.point.y = targetPosition.point.y
    if (goto.useDone) {
      this.engine.addComponent(entity, new GoToDoneComponent())
    } else {
      this.engine.removeComponent(entity, GoToComponent)
    }
  }
}
