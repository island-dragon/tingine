import { Rect } from "../Graphics/Rect"

export default class V2D {
  public static rotate = (radian: number, point: V2D): V2D => {
    const cos = Math.cos(radian)
    const sin = Math.sin(radian)

    return new V2D(
      point.x * cos - point.y * sin,
      point.x * sin + point.y * cos,
    )
  }

  public static pointWithinRect = (point: V2D, rect: Rect) => point.x > rect.x
    && point.x < rect.x + rect.w
    && point.y > rect.y
    && point.y < rect.y + rect.h


  public static rad2dir = (radian: number): V2D =>
    new V2D(Math.cos(radian), Math.sin(radian))

  public static distance = (a: V2D, b: V2D): number => {
    const dx = a.x - b.x
    const dy = a.y - b.y
    return Math.sqrt(dx * dx + dy * dy)
  }

  public static add = (a: V2D, b: V2D): V2D =>
    new V2D(a.x + b.x, a.y + b.y)

  constructor(
    private _x: number = 0,
    private _y: number = 0,
    public relative?: V2D
  ) { }

  public get x() {
    return this._x + (this.relative?.x ?? 0)
  }

  public get y() {
    return this._y + (this.relative?.y ?? 0)
  }

  public set x(x: number) { this._x = x - (this.relative?.x ?? 0) }
  public set y(y: number) { this._y = y - (this.relative?.y ?? 0) }

  public normalize(): V2D {
    const res = new V2D(this.x, this.y)
    let len = res.x * res.x + res.y * res.y

    if (len > 0) {
      len = 1 / Math.sqrt(len)

      res.x = res.x * len
      res.y = res.y * len
    }

    return res
  }

  public set(x: number, y: number): void {
    this.x = x
    this.y = y
  }

  public clone = (point: V2D): void =>
    this.set(point.x, point.y)

  public lerp = (target: V2D, t: number = 0) =>
    new V2D(t * (target.x - this.x), t * (target.y - this.y))

  public sub = (target: V2D) =>
    new V2D(target.x - this.x, target.y - this.y)

  public mul = (val: number) =>
    new V2D(this.x * val, this.y * val)

  public apply = (val: V2D) =>
    this.set(this.x + val.x, this.y + val.y)

  public toTileSpace = (tileWidth: number) =>
    new V2D(Math.floor(this.x / tileWidth), Math.floor(this.y / tileWidth))

  public distanceTo(target: V2D): number {
    const x = Math.abs(target.x - this.x)
    const y = Math.abs(target.y - this.y)
    return 1.426776695 * Math.min(0.7071067812 * (x + y), Math.max(x, y))
  }
}
