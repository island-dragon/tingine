export const shuffle = (arr: any[]) => {
  const out: any[] = []
  for (let i = 0; i < arr.length; i++) {
    Array.prototype[(Math.random() > 0.5) ? 'push' : 'unshift'].call(out, arr[i])
  }

  return out
}
