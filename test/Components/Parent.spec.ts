import { expect } from 'chai'
import 'mocha'
import { Parent } from '../../lib/Components/Parent'
import { Position2D } from '../../lib/Components/Position2D'

describe('Parent', () =>
  [
    context('when displayed', () =>
      it('should display a usable representation of where it points to', () =>
        expect((new Parent(45)).toString()).to.equal('Parent(45)')
      )
    ),
    context('when instanciated without parameters', () =>
      it('should initializes to 0,0', () =>
        expect(new Parent(3))
          .property('globalPoint')
          .property('point')
          .to.include({ x: 0, y: 0 })
      )
    ),
    context('when instanciated', () =>
      it('should set the proper value property', () =>
        expect(new Parent(3)).to.include({ value: 3 })
      )
    ),
    context('when instanciated with 10,10 as parameters', () =>
      it('should initializes to 10,10', () =>
        expect(new Parent(3, new Position2D(10, 10)))
          .property('globalPoint')
          .property('point')
          .to.include({ x: 10, y: 10 })
      )
    )
  ]
)
