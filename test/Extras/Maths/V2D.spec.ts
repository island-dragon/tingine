import { expect } from 'chai'
import 'mocha'
import V2D from '../../../lib/Extras/Maths/V2D'

describe('V2D', () =>
  [
    context('when instanciated without parameters', () =>
      it('should initializes to 0,0', () =>
        expect(new V2D()).to.include({x: 0, y: 0})
      )
    ),
    context('when instanciated with 0 as parameters', () =>
      it('should initializes to 0,0', () =>
        expect(new V2D(0, 0)).to.include({ x: 0, y: 0 })
      )
    )
  ]
)
