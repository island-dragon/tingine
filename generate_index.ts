// tslint:disable: no-console

import { readdirSync, writeFileSync } from 'fs'

const rootPath = __dirname + '/lib'

console.info('Adding index files to:')

const generateIndexTs = (root: string = rootPath) => {
  const basePath = root.replace(new RegExp(`^${rootPath}/`), '')

  console.info(
    root === rootPath
      ? `.`
      : `${basePath}`
  )

  const items = readdirSync(root)
    .filter(i => i.indexOf('index.ts') === -1)
    .map(i => i.indexOf('.ts') === -1 ? (generateIndexTs(`${root}/${i}`), i) : i)
    .map(i => i.replace(/\.ts$/, ''))
    .map(i => `export * from './${i}'`)
  items.push('')

  writeFileSync(`${root}/index.ts`, items.join('\n'))
}

generateIndexTs()
